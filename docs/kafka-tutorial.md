Kafka Tutorial
========

# Introduction
The purpose of this tutorial is to demonstrate how to write a **producer** and **consumer** in **Kafka**. This tutorial provides:
- simple consumer and producer for **Kafka**
- containerised consumer and producer for **Kafka**
- orchestrated consumer and producer for **Kafka**

Note that all the source codes used in this tutorial are available in the `code-samples` folder.

# Kafka Installation and Setup
The installation guide for [Apache Kafka](https://kafka.apache.org/) is available on the website.

Once you complete the installation, you can start it as follows:

`bin/zookeeper-server-start.sh config/zookeeper.properties`

`bin/kafka-server-start.sh config/server.properties`

Finally, using **Kafka** as a communication medium, you need to create the topics. For example, `kafka-topics --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic dsp` creates a topic called dsp.

# Producer and Consumer 

In this first step, we will implement a simple producer and consumer to interact through **Kafka**.

`ballerina new simple-kafka-example`
`ballerina add consumer`
`ballerina add producer`

Once you are done writing the code, compile with `ballerina build -a --skip-tests`.

Finally, run the programmes as follows:
`ballerina run target/bin/consumer.jar`

`ballerina run target/bin/producer.jar`

The message (a JSON object) sent from the producer will now be displayed in the terminal where the consumer is being executed.

# Containerisaton
Recreate a new project and create the consumer and producer modules. Then copy the content of the producer and consumer code from the previous example. In each source file, import the `ballerina/docker` module. Then add docker configuration as follows:
`@docker:Config {
	name: "cconsumer",
	tag: "v1.0"
}
`
You should change the name and tag in each file. Finally, compile the project and run the docker instances:

`ballerina build -a --skip-tests`

`docker run -d cconsumer:v1.0` and `docker run -d cproducer:v1.0`

Note that the execution inside the container will obviously not display on the terminal; the container has isolated the service or the programme. Use docker commands to remove and clean the instances. Also, note that you need **docker** installed on the machine to start the container instances.

# Orchestration
Here we will use **Kubernetes** (`k8s`) to orchestrate the whole application. Please note, that `k8s` will automatically create docker containers for you. First, make sure you install a `k8s` server (e.g., **minikube**) on your machine.

Recreate a new project and add the modules as in previous cases. Copy the consumer and producer codes in the respective files. In each source file, import the `ballerina/docker` module. Then add docker configuration as follows:
`@kubernetes:Deployment {
    image:"consumer-service",
    name:"kafka-consumer"
}`

You should rename each service. Finally, compile the project and run the docker instances:

`ballerina build -a --skip-tests` 

To deploy, follow the instruction issued by the build process. For example, `kubectl apply -f orch-kafka-example/target/kubernetes/producer`

You can now use the `kubectl` command to inspect the deployment and the containers.

# Integrating GraphQl and Kafka
The example in `code-samples/kafka-graphql-example` demonstrates how kafka clients can be integrated with GraphQL. The program shows a typical GraphQL service with a **Kafka** producer. Whenever an API call is received, the producer then produces a message and sends it through **Kafka**.