# Lab 2

In this lab, we will implement a service in *Ballerina* with a restful API to interact with it. The API is specified in the file users-api.yaml.

## Problem 1

Using the openapi tool in **Ballerina** language, generate a users service.

## Problem 2

Implement the users service with the following functionality:
1. Create a new user;
1. View an existing user;
1. Update a field in an existing user;
1. Delete an existing user.

A user can be represented by her first and last names, her email address and profile. The identifier attached to a user should be returned after creation.

Note that you are expected to create a git repository for your package.
